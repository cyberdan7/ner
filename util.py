import codecs
import json
import numpy as np
import tensorflow as tf
import os
import sys
import shutil


SEQSEPARATOR = '\n\n'

class DataProcessor():
    def __init__(self, input_vocab_path=None, output_vocab_path=None, chars_vocab_path=None):
        self.input_vocab = {'_PADDING_':0, 'UNKNOWN':1}
        self.output_vocabs = {}

        self.chars_vocab = {'_PADDING_':0, 'UNKNOWN':1}

        self.caps_vocab = {'UPPER':0, 'LOWER':1}

        if input_vocab_path is not None:
            self.load_input_vocab(input_vocab_path)
        if output_vocab_path is not None:
            self.load_output_vocabs(output_vocab_path)

        if chars_vocab_path is not None:
            self.load_chars_vocabs(chars_vocab_path)

    def load_input_vocab(self, path):
        with codecs.open(path) as input_file:
            self.input_vocab = json.loads(input_file.read())

    def load_output_vocabs(self, path):
        with codecs.open(path) as input_file:
            self.output_vocabs = json.loads(input_file.read())

    def load_chars_vocabs(self, path):
        with codecs.open(path) as input_file:
            self.chars_vocab = json.loads(input_file.read())

    def load_caps_vocabs(self, path):
        with codecs.open(path) as input_file:
            self.caps_vocab = json.loads(input_file.read())

    def save_vocabs(self, input_vocab_path, output_vocab_path, chars_vocab_path=None, caps_vocab_path=None):
        with codecs.open(input_vocab_path, 'w') as output_file:
            output_file.write(json.dumps(self.input_vocab))
        with codecs.open(output_vocab_path, 'w') as output_file:
            output_file.write(json.dumps(self.output_vocabs))
        if chars_vocab_path is not None:
            with codecs.open(chars_vocab_path, 'w') as output_file:
                output_file.write(json.dumps(self.chars_vocab))
        if caps_vocab_path is not None:
            with codecs.open(caps_vocab_path, 'w') as output_file:
                output_file.write(json.dumps(self.caps_vocab))


    def map_sequences_with_vocab(self, sequences, vocab, update=True):
        mapped_sequences = []
        for sequence in sequences:
            mapped_sequence = []
            for token in sequence:
                if update and token not in vocab: vocab[token] = len(vocab)
                token_id = vocab[token] if token in vocab else vocab['UNKNOWN']
                mapped_sequence.append(token_id)
            mapped_sequences.append(mapped_sequence)

        return mapped_sequences

    def map_out_sequences_with_vocab(self, sequences, vocab, update=True):
        mapped_sequences = []
        for sequence in sequences:
            mapped_sequence = []
            for token in sequence:
                if update and token not in vocab: vocab[token] = len(vocab)
                token_id = vocab[token] if token in vocab else vocab['O']
                mapped_sequence.append(token_id)
            mapped_sequences.append(mapped_sequence)

        return mapped_sequences

    def map_cap_sequences_with_vocab(self, sequences, vocab, update=True):
        mapped_sequences = []
        for sequence in sequences:
            mapped_sequence = []
            for token in sequence:
                if token[0].isupper():
                    token_id = vocab['UPPER']
                else:
                    token_id = vocab['LOWER']
                mapped_sequence.append(token_id)
            mapped_sequences.append(mapped_sequence)

        return mapped_sequences

    def map_insequencescap(self, insequences, update=True):
        return self.map_cap_sequences_with_vocab(insequences, self.caps_vocab, update)

    def map_chars_in_sequences_with_vocab(self, sequences, vocab, update=True):
        mapped_chars_in_sequences = []
        for sequence in sequences:
            mapped_chars_in_sequence = []
            for token in sequence:
                mapped_chars = []
                for ch in token:
                    if update and ch not in vocab: vocab[ch] = len(vocab)
                    token_id = vocab[ch] if ch in vocab else vocab['UNKNOWN']
                    mapped_chars.append(token_id)
                mapped_chars_in_sequence.append(mapped_chars)
            mapped_chars_in_sequences.append(mapped_chars_in_sequence)

        return mapped_chars_in_sequences

    def map_insequences(self, insequences, update=True):
        return self.map_sequences_with_vocab(insequences, self.input_vocab, update)

    def map_inchars(self, insequences, update=True):
        return self.map_chars_in_sequences_with_vocab(insequences, self.chars_vocab, update)

    def map_caps_in_sequences_with_vocab(self, sequences, vocab, update=True):
        mapped_chars_in_sequences = []
        for sequence in sequences:
            mapped_chars_in_sequence = []
            for token in sequence:
                mapped_chars = []
                for ch in token:
                    if ch.isupper():
                        token_id = vocab['UPPER']
                    else:
                        token_id = vocab['LOWER']
                    mapped_chars.append(token_id)
                mapped_chars_in_sequence.append(mapped_chars)
            mapped_chars_in_sequences.append(mapped_chars_in_sequence)

        return mapped_chars_in_sequences

    def map_incaps(self, insequences, update=True):
        return self.map_caps_in_sequences_with_vocab(insequences, self.caps_vocab, update)

    def map_outsequences(self, outsequences, task_id, update=True):
        if str(task_id) not in self.output_vocabs:
            self.output_vocabs[str(task_id)] = {'O':0} # '_PADDING_':0
        return self.map_out_sequences_with_vocab(outsequences, self.output_vocabs[str(task_id)], update)

    def map_back(self, y_pred, task_id):
        return self.map_sequences_with_vocab(y_pred, self.reverse_vocabs[str(task_id)], update=False)

    def map_seq_back(self, seq, task_id):
        return self.map_sequences_with_vocab(seq, self.reversed_input_vocab, update=False)

    def reverse_vocabularies(self):
        self.reverse_vocabs = {}
        for task_id, vocab in self.output_vocabs.items():
            self.reverse_vocabs[task_id] = {item:key for key, item in vocab.items()}
        self.reversed_input_vocab = {item:key for key, item in self.input_vocab.items()}


def data_batcher(dataset, batch_size, shuffle=True, masked_len=False):
    """
        dataset is array of (task_id, in_seq, out_seq, lens, in_words, words_lens, in_caps)}
        returns batches in_seq, out_seq, lens, task_id, in_words, words_lens, in_caps
    """
    in_words_batches = []
    words_lens_batches = []
    in_caps_batches = []
    in_seq_batches = []
    out_seq_batches = []
    lens_batches = []
    task_ids = []
    total_batches_cnt = 0
    for task_id, in_seq, out_seq, lens, in_words, words_lens, in_caps in dataset:
        ds_size = len(in_seq)
        batches_cnt = int(np.floor(1.0 * ds_size/batch_size))
        total_batches_cnt += batches_cnt

        in_words_batches += [in_words[i*batch_size:min(ds_size, (i+1)*batch_size)] for i in range(batches_cnt)]
        #
        words_lens_batches += [words_lens[i*batch_size:min(ds_size, (i+1)*batch_size)] for i in range(batches_cnt)]
        #
        in_caps_batches += [in_caps[i*batch_size:min(ds_size, (i+1)*batch_size)] for i in range(batches_cnt)]
        #
        in_seq_batches += [in_seq[i*batch_size:min(ds_size, (i+1)*batch_size)] for i in range(batches_cnt)]
        #np.array_split(in_seq, batches_cnt)
        out_seq_batches += [out_seq[i*batch_size:min(ds_size, (i+1)*batch_size)] for i in range(batches_cnt)]
        #np.array_split(out_seq, batches_cnt)
        lens_batches += [lens[i*batch_size:min(ds_size, (i+1)*batch_size)] for i in range(batches_cnt)]
        #np.array_split(lens, batches_cnt)
        task_ids += [task_id]*batches_cnt

    permutation = np.arange(total_batches_cnt)

    def batcher():
        if shuffle: permutation = np.random.permutation(total_batches_cnt)
        for i in range(total_batches_cnt):
            batch_id = permutation[i]

            in_words = np.array(in_words_batches[batch_id])
            words_lens = np.array(words_lens_batches[batch_id])
            in_caps = np.array(in_caps_batches[batch_id])
            if masked_len: words_lens = len_to_mask(in_words.shape[2], words_lens)

            in_seq = np.array(in_seq_batches[batch_id])
            out_seq = np.array(out_seq_batches[batch_id])
            lens = np.array(lens_batches[batch_id])
            if masked_len: lens = len_to_mask(in_seq.shape[1], lens)
            task_id = task_ids[batch_id]
            yield in_seq, out_seq, lens, task_id, in_words, words_lens, in_caps

    return total_batches_cnt, batcher


def read_datasets(paths_to_datasets, seqsep=SEQSEPARATOR):
    """
    :param paths_to_datasets: is a list of paths to the datasets
    :param seqsep: is a string that is used to separate sequences(examples) in datasets
    :return:
            list of [
                        task_id --
                        input_sequences --
                        output_sequences --
                        input_lens --
                    ]
    """
    datasets = []
    for task_id, train_path in enumerate(paths_to_datasets):
        with codecs.open(train_path, encoding='utf8') as input_file:
            ds = input_file.read().split(seqsep)

        ds = [[line.split('\t') for line in seq.split('\n') if line!='' and line !='\n'] for seq in ds]

        if ds[0][0][0] == '-DOCSTART-':
            del ds[0]
			
        if ds[-1] == []:
            del ds[-1]

        input_sequences = [[s[0].split(' ')[0] for s in seq] for seq in ds]
        input_lens = list(map(len, input_sequences))
        output_sequences = [[s[0].split(' ')[1] for s in seq] for seq in ds]

        input_char_lens = []
        for s in input_sequences:
            tmp = []
            for w in s:
                tmp.append(len(w))
            input_char_lens.append(tmp)

        datasets.append(
                        [
                            task_id,
                            input_sequences,
                            output_sequences,
                            input_lens,
                            input_char_lens
                        ]
                    )
    return datasets


def train_test_split(dataset, sp=0.8):
    train_dataset = [dataset[0], dataset[1][:int(len(dataset[1])*sp)], dataset[2][:int(len(dataset[2])*sp)], dataset[3][:int(len(dataset[3])*sp)], dataset[4][:int(len(dataset[4])*sp)]]
    test_dataset = [dataset[0], dataset[1][int(len(dataset[1])*sp):], dataset[2][int(len(dataset[2])*sp):], dataset[3][int(len(dataset[3])*sp):], dataset[4][int(len(dataset[4])*sp):]]
    return train_dataset, test_dataset


def get_embeddings(vocabulary, w2v_model={}, embedding_dim=100):
    vocab_size = len(vocabulary)
    embeddings = np.random.uniform(-np.sqrt(3/embedding_dim), np.sqrt(3/embedding_dim), (vocab_size, embedding_dim))
    for token, index in vocabulary.items():
        if token in w2v_model: embeddings[index] = w2v_model[token]

    return embeddings.astype(np.float32)


def pad_sequence(sequence, length, padding_value=0):
    if len(sequence) < length:
        return sequence[:length] + [padding_value]*(length - len(sequence))
    else:
        return sequence[:length]


def pad_sequences(ds, length, padding_value=0):
    pad_sequence_to_length = lambda sequence: pad_sequence(sequence, length, padding_value)
    ds[1] = list(map(pad_sequence_to_length, ds[1]))
    ds[2] = list(map(pad_sequence_to_length, ds[2]))


def pad_char(sequence, length, seq_len, padding_value=0):
    ans = []
    for char in sequence:
        if len(char) < length:
            ans.append(char[:length] + [padding_value]*(length - len(char)))
        else:
            ans.append(char[:length])
    while len(ans) < seq_len:
        ans.append([padding_value]*length)
    ans = ans[:seq_len]
    return ans

def pad_chars(ds, length, seq_len, padding_value=0):
    pad_char_to_length = lambda sequence: pad_char(sequence, length, seq_len, padding_value)
    ds[5] = list(map(pad_char_to_length, ds[5]))
    ds[6] = list(map(pad_char_to_length, ds[6])) # if caps instead of seq caps 


def truncate_lens(ds, input_max_len):
    ds[3] = map(lambda length: length if length < input_max_len else input_max_len, ds[3])


def find_all_entities(t_list):
    find = []
    b = -1
    f = ''
    for i, y in enumerate(t_list):
        if y == 'O':
            if b != -1:
                find.append([b, i-1, f])
                b = -1
                
        if y[0] == 'B':
            if b == -1:
                b = i
                f = y[2:]
            else:
                find.append([b, i-1, f])
                
                b = i
                f = y[2:]
                
    if b != -1:
        find.append([b, i, f])
                
    return np.array(find)

        
def evaluate(network, dataset, metric, dp):
    for task_id, input_seq, y_true, input_lens, input_char, input_char_lens, input_caps in dataset:
        print('Evaluating on {} task'.format(task_id))
        print('-'*50)
        sys.stdout.flush()
        y_pred = network.bpredict(task_id, input_seq, input_lens, input_char, input_char_lens, input_caps)
        y_pred = dp.map_back(y_pred, task_id)
        y_true = dp.map_back(y_true, task_id)

        fpath = 'predc_tmp.csv'
        save_in_conll(input_seq, y_true, y_pred, fpath)
        os.system('perl conlleval < {}'.format(fpath))
        print('='*50)
        sys.stdout.flush()


def save_in_conll(input_seq, y_true, y_pred, fpath):
    with codecs.open(fpath, 'w') as output_file:
        for tokens, true_labels, pred_labels in zip(input_seq, y_true, y_pred):
            for token, true_label, pred_label in zip(tokens, true_labels, pred_labels):
                output_file.write('{} {} {}\n'.format(token, true_label, pred_label))
            output_file.write('\n')


def len_to_mask(input_max_len, lens):
    bsize = lens.shape[0]
    mask = np.zeros((input_max_len, bsize))
    for length in lens:
        mask[:length] = 1
    return mask


def config_from_file(path_to_conf):
    with codecs.open(path_to_conf) as input_file:
        raw_conf_str = input_file.read()
    return json.loads(raw_conf_str)


def save_config(path_to_conf, config):
    dumped_config = json.dumps(config)
    with codecs.open(path_to_conf, 'w') as output_file:
        output_file.write(dumped_config)


def conll_format_to_json(tag_sequences, token_sequences):
    extracted = []
    for tag_sequence, token_sequence in zip(tag_sequences, token_sequences):
        entity = []
        entity_type = None
        json_data = []
        for tag, token in zip(tag_sequence, token_sequence):
            if entity_type is not None and not tag.startswith('I'):
                json_data.append({'entity': ' '.join(entity),
                                  'entity_type': entity_type})
                entity = []
                entity_type = None
            if tag.startswith('B-') or tag.startswith('I-'):
                entity.append(token)
                entity_type = tag[2:]

        if entity_type is not None:
            json_data.append({'entity': ' '.join(entity),
                              'entity_type': entity_type})
        extracted.append(json_data)
    return extracted


def correct_conll_format(tag_sequences):
    tag_sequences_ = []
    for tag_sequence in tag_sequences:
        tag_sequence_ = []
        prev_tag = tag_sequence[0]
        for tag in tag_sequence:
            if prev_tag.startswith('O') and tag.startswith('I-'): tag = 'B-' + tag[2:]
            tag_sequence_.append(tag)
            prev_tag = tag
        tag_sequences_.append(tag_sequence_)

    return tag_sequences_

