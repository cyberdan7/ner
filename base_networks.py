import tensorflow as tf
from util import data_batcher
import progressbar


def weights_variable(shape):
    initial = tf.contrib.layers.xavier_initializer()
    return tf.Variable(initial(shape))

class BaseCRFDN(object):
    def __init__(self, input_max_len, word_max_len, tasks_count, embeddings, char_embeddings, caps_embeddings, lr):
        self.input_max_len = input_max_len
        self.tasks_count = tasks_count
        self.embeddings = tf.Variable(embeddings, dtype=tf.float32) # , trainable=False
        self.char_embeddings = tf.Variable(char_embeddings, dtype=tf.float32)
        self.caps_embeddings = tf.Variable(caps_embeddings, dtype=tf.float32)

        self.input_sequence = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        self.input_lens = tf.placeholder(dtype=tf.int32, shape=[None])

        self.input_words = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len, word_max_len])
        self.words_lens = tf.placeholder(tf.int32, shape=[None, input_max_len])

        self.input_caps = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len, word_max_len])

        self.output_sequences = tf.placeholder(dtype=tf.int32, shape=(None, input_max_len))

        self.lr = lr

    def _inference(self): raise NotImplementedError()

    def _losses(self):
        losses = []
        self.trans_params = []
        for task in range(self.tasks_count):
            with tf.variable_scope('task_{}_crf'.format(task)):
                log_likelihood, trans_params = tf.contrib.crf.crf_log_likelihood(self.logits[task], self.output_sequences, self.input_lens)
                losses.append(tf.reduce_mean(-log_likelihood))
                self.trans_params.append(trans_params)

        return losses

    def _optimizers(self):
        global_step = tf.Variable(0, trainable=False)
        learning_rate = tf.train.exponential_decay(0.003, global_step, 100000, 0.96, staircase=True)

        losses = self.lr
        ops = []
        for i in range(self.tasks_count):
            #l2_loss = tf.reduce_sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
            optimizer = tf.train.AdamOptimizer(losses[i])
            gradients, variables = zip(*optimizer.compute_gradients(self.losses[i])) # + l2_loss*0.0001
            gradients, _ = tf.clip_by_global_norm(gradients, 1.0) 
            train_op = optimizer.apply_gradients(zip(gradients, variables), global_step=global_step)
            ops.append(train_op)
        return ops

    def train_network(self, train_set, val_set, epochs, batch_size, callbacks=[]):
        """
            input is a sequence of word ids
            output is a sequence of label ids
        """

        self.batch_size = batch_size

        batches_cnt, batcher = data_batcher(train_set, batch_size)
        pbar = progressbar.ProgressBar(maxval=batches_cnt, widgets=[
                                        progressbar.DynamicMessage('Epoch'),
                                        progressbar.Bar(),
                                        progressbar.AdaptiveETA(),
                                    ]
                                )

        for epoch in range(epochs):
            print('EPOCH {}'.format(epoch+1))
            pbar.start()
            pbar.update(0, Epoch=epoch+1)

            for batch_id, batch in enumerate(batcher()):
                task_id = batch[3]
                input_seq = batch[0]
                input_lens = batch[2]
                output_seq = batch[1]

                input_words = batch[4]
                input_words_lens = batch[5]

                input_caps = batch[6]

                optimize_op = self.optimize_ops[task_id]
                loss = self.losses[task_id]
                feed_dict = self.check_feeds(
                                                task_id,
                                                input_seq,
                                                input_lens,
                                                input_words,
                                                input_words_lens,                                            
                                                input_caps,
                                                output_seq
                                            )
                _, loss_values = self.sess.run([optimize_op, loss], feed_dict=feed_dict)

                pbar.update(batch_id, Epoch=epoch+1)


            pbar.finish()
            for callback in callbacks:
                callback(self, val_set)

    def predict(self, task_id, input_seq, input_lens, input_words, input_words_lens, input_caps):
        feed_dict = self.check_feeds(task_id, input_seq, input_lens, input_words, input_words_lens, input_caps)
        viterbi_sequences = []
        logits, trans_params = self.sess.run([self.logits[task_id], self.trans_params[task_id]], feed_dict=feed_dict)

        for logit, sequence_length in zip(logits, input_lens):
            logit = logit[:sequence_length]

            if len(logit) == 0:
                return viterbi_sequences

            viterbi_seq, viterbi_score = tf.contrib.crf.viterbi_decode(logit, trans_params)
            viterbi_sequences += [viterbi_seq]
           
        return viterbi_sequences


    def bpredict(self, task_id, input_seq, input_lens, input_words, input_words_lens, input_caps, bsize=None):
        if bsize == None:
            bsize = self.batch_size
        datasize = len(input_seq)
        bcount = int((datasize + bsize - 1)/bsize)
        y_pred = []
        for bidx in range(bcount):
            bstart = bsize*bidx
            bend = min(datasize, bsize*(bidx+1))
            y_pred += self.predict(task_id, input_seq[bstart:bend], input_lens[bstart:bend], input_words[bstart:bend], input_words_lens[bstart:bend], input_caps[bstart:bend])

        return y_pred

    def check_feeds(self, task_id, input_seq, input_lens, input_words, input_words_lens, input_caps, output_seq=None):
        return {
                    self.input_sequence:input_seq,
                    self.input_lens:input_lens,
                    self.output_sequences:output_seq,
                    self.input_words:input_words,
                    self.words_lens:input_words_lens,
                    self.input_caps:input_caps
                } if output_seq is not None else \
                {
                    self.input_sequence:input_seq,
                    self.input_lens:input_lens,
                    self.input_words:input_words,
                    self.words_lens:input_words_lens,
                    self.input_caps:input_caps
                }

    def save(self, fpath): tf.train.Saver().save(self.sess, fpath)

    def restore_from_file(self, fpath): tf.train.Saver().restore(self.sess, fpath)
