# coding: utf-8

import os
from util import *
from networks import LSTMCRF
from gensim.models import KeyedVectors, FastText
from metrics import exact_matching


BSIZE = 64
EPOCHS = 10
learning_rates = [0.001, 0.001, 0.003] # learning rate for each task_id starting at 0 (if the number of tasks is less, extra will not be used)
load = False # load saved weights
train = True # train or only evaluate
USE_SPLIT = False # if False then 80/20 split will be apply otherwise ready split list expected 
W2V_PATH = 'lenta_lower_100.bin' # set None to use random initialization
WORDS_DIM = 100 # words embeddings dimention
CHARS_DIM = 25 # chars embeddings dimention
CAPS_DIM = 10 # caps embedding dimention

### Loading and Preprocessing Datasets

if USE_SPLIT == True:
    train_datasets = read_datasets([r'factru16\factrueval_train.txt']) # List of train datasets
    test_datasets = read_datasets([r'factru16\factrueval_test.txt']) # List of test datasets
else:
    datasets = read_datasets([r'gareev\gareev_sp.txt', r'collection5\total.txt']) # List of datasets

    train_datasets = []
    test_datasets = []
    # Train/test split
    for dataset in datasets:
        train_dataset, test_dataset = train_test_split(dataset, 0.8)
        train_datasets.append(train_dataset)
        test_datasets.append(test_dataset)
    datasets = []

TASKS_COUNT = len(train_datasets)

data_processor = DataProcessor()

# reading train/test datasets
# *_dataset in list where each item consist of:
#                                               1. task_id:int -- id of the task
#                                               2. input sequence:list of str -- tokenized input text
#                                               3. output sequence:list of str -- tag of each token
#                                               4. sequence lengths:int -- length of the text
#                                               5. input characters:int -- tokenized input characters
#                                               6. words lengths:int -- length of each word in sequence
#                                               7. input caps:int -- for each letter is it big or not

input_max_len = 0
char_max_len = 0
# Preprocessing train dataset
for dataset in train_datasets:
    task_id = dataset[0]
    
    # Add caps tokens for each char
    caps = data_processor.map_incaps(dataset[1])

    # Lowercase
    for i, s in enumerate(dataset[1]):
        for j, w in enumerate(s):
            dataset[1][i][j] = w.lower()

    # Replacing chars tokens with correspinding token ids from token vocabulary
    dataset.append(data_processor.map_inchars(dataset[1]))
    dataset.append(caps)
    
    # Replacing tokens with correspinding token ids from token vocabulary
    dataset[1] = data_processor.map_insequences(dataset[1])
    
    # Replacing tags with corresponding tag ids from tag vocabulary corresponding to the task id
    dataset[2] = data_processor.map_outsequences(dataset[2], task_id)
    
    # Finding maximal length of the input sequence
    ds_input_max_len = max(dataset[3])
    ds_char_max_len = max(list(map(max, dataset[4])))
    if task_id == 0:
        if ds_input_max_len > input_max_len: input_max_len = ds_input_max_len
        if ds_char_max_len > char_max_len: char_max_len = ds_char_max_len


# Here you can change lengths:
#input_max_len = 40
#char_max_len = 15

# Apply padding/truncating
for dataset in train_datasets:
    pad_sequences(dataset, input_max_len)

for dataset in train_datasets:
    pad_chars(dataset, char_max_len, input_max_len)

for dataset in train_datasets:
    for i, s in enumerate(dataset[4]):
        dataset[4][i] = dataset[4][i][:input_max_len]
        while len(dataset[4][i]) < input_max_len:
            dataset[4][i].append(0)


# Preprocessing test dataset
for dataset in test_datasets:
    task_id = dataset[0]
    
    # Add caps tokens for each char
    caps = data_processor.map_incaps(dataset[1])

    # Lowercase
    for i, s in enumerate(dataset[1]):
        for j, w in enumerate(s):
            dataset[1][i][j] = w.lower()

    # Replacing chars tokens with correspinding token ids from token vocabulary
    dataset.append(data_processor.map_inchars(dataset[1]))
    dataset.append(caps)
    
    # Replacing tokens with correspinding token ids from token vocabulary without updating vocabulary
    dataset[1] = data_processor.map_insequences(dataset[1]) # , update=False
    
    # Replacing tags with corresponding tag ids from tag vocabulary corresponding to the task id
    dataset[2] = data_processor.map_outsequences(dataset[2], task_id, update=False)
    
    # padding/truncating sequence to the input_max_len 
    pad_sequences(dataset, input_max_len)
    pad_chars(dataset, char_max_len, input_max_len)


for dataset in test_datasets:
    for i, s in enumerate(dataset[4]):
        dataset[4][i] = dataset[4][i][:input_max_len]
        while len(dataset[4][i]) < input_max_len:
            dataset[4][i].append(0)


# Reverting vocabularies for future usage
data_processor.reverse_vocabularies()

for dataset in train_datasets:
    dataset[5], dataset[4] = dataset[4], dataset[5]

for dataset in test_datasets:
    dataset[5], dataset[4] = dataset[4], dataset[5]


# Loading word2vec vectors if defined, otherwise randomly initialized 

w2v_model = {}
if W2V_PATH is not None:
    w2v_model = FastText.load_fasttext_format(W2V_PATH, encoding='utf8')
embeddings = get_embeddings(data_processor.input_vocab, w2v_model=w2v_model, embedding_dim=WORDS_DIM)

char_w2v_model = {}
char_embeddings = get_embeddings(data_processor.chars_vocab, w2v_model=char_w2v_model, embedding_dim=CHARS_DIM)

caps_w2v_model = {}
caps_embeddings = get_embeddings(data_processor.caps_vocab, w2v_model=caps_w2v_model, embedding_dim=CAPS_DIM)


# Creating the network

# calculating output size for each task
classes_counts = [len(data_processor.output_vocabs[str(task_id)]) for task_id in range(TASKS_COUNT)]


network = LSTMCRF(input_max_len=input_max_len,
                  tasks_count=TASKS_COUNT,
                  embeddings=embeddings,
                  classes_counts=classes_counts,
                  word_max_len=char_max_len,
                  char_embeddings= char_embeddings,
                  caps_embeddings = caps_embeddings,
                  lr=learning_rates
                  )


# Load
if load == True:
    network.restore_from_file('models/ner_model.ckpt')
    data_processor.load_input_vocab('models/vocabs/input_vocab')
    data_processor.load_output_vocabs('models/vocabs/output_vocabs')
    data_processor.load_chars_vocabs('models/vocabs/char_vocabs')
    data_processor.load_caps_vocabs('models/vocabs/caps_vocabs')


# Train
if train == True:
    network.train_network(train_set=train_datasets, 
                      val_set=test_datasets, 
                      epochs=EPOCHS, 
                      batch_size=BSIZE, 
                      callbacks=[lambda net, dataset: evaluate(net, dataset, exact_matching, data_processor)])
else:
    evaluate(network, train_datasets, exact_matching, data_processor)


# Saving network, vocabularies and configuration
if train == True:
    data_processor.save_vocabs(input_vocab_path='models/vocabs/input_vocab',
                               output_vocab_path='models/vocabs/output_vocabs',
                               chars_vocab_path='models/vocabs/char_vocabs',
                               caps_vocab_path='models/vocabs/caps_vocabs')
    network.save('models/ner_model.ckpt')

    config = {'input_max_len':input_max_len,
              'layers':LAYERS,
              'tasks_count':TASKS_COUNT,
              'classes_coubts':classes_counts}
    save_config('models/ner_model_config', config)
