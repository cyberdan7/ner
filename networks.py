from base_networks import *

def variational_dropout(units, keep_prob, fixed_mask_dims=(1,)):
    """ Dropout with the same drop mask for all fixed_mask_dims
    Args:
        units: a tensor, usually with shapes [B x T x F], where
            B - batch size
            T - tokens dimension
            F - feature dimension
        keep_prob: keep probability
        fixed_mask_dims: in these dimensions the mask will be the same
    Returns:
        dropped units tensor
    """
    units_shape = tf.shape(units)
    noise_shape = [units_shape[n] for n in range(len(units.shape))]
    for dim in fixed_mask_dims:
        noise_shape[dim] = 1

    return tf.nn.dropout(units, keep_prob, noise_shape)


def get_rnn_lstm(layers_units):
    create_cell = tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell
    return tf.contrib.rnn.MultiRNNCell([
                                            create_cell(units)
                                            for units in layers_units
                                        ]), \
           tf.contrib.rnn.MultiRNNCell([
                                            create_cell(units)
                                            for units in layers_units
                                        ])


class LSTMCRF(BaseCRFDN):
    def __init__(self, input_max_len, word_max_len, tasks_count, embeddings, char_embeddings, caps_embeddings, classes_counts, lr):
        super(LSTMCRF, self).__init__(input_max_len, word_max_len, tasks_count, embeddings, char_embeddings, caps_embeddings, lr)
        self.classes_counts = classes_counts

        self.lstm_cell = [tf.nn.rnn_cell.LSTMCell(25, initializer=tf.orthogonal_initializer(), state_is_tuple=True), tf.nn.rnn_cell.LSTMCell(25, initializer=tf.orthogonal_initializer(), state_is_tuple=True)]

        self.bidir_lstm = [tf.nn.rnn_cell.LSTMCell(100, initializer=tf.orthogonal_initializer()), tf.nn.rnn_cell.LSTMCell(100, initializer=tf.orthogonal_initializer())] 

        kernels = [var for var in self.bidir_lstm[0].trainable_variables + self.bidir_lstm[1].trainable_variables if 'kernel' in var.name]
        for kernel in kernels:
            tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES, tf.nn.l2_loss(kernel))

        self.logits = self._inference()
        self.losses = self._losses()
        self.optimize_ops = self._optimizers()
        self.predict_ops = [tf.argmax(tslogits, axis=-1) for tslogits in self.logits]
        gpu_options = tf.GPUOptions(allow_growth=True)
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, gpu_options=gpu_options))
        self.sess.run(tf.global_variables_initializer())

    def _inference(self):
        with tf.name_scope('embeddings'):
            embedded_sequence = tf.nn.embedding_lookup(self.embeddings, self.input_sequence)

            embedded_words = tf.nn.embedding_lookup(self.char_embeddings, self.input_words)
            s = tf.shape(embedded_words)
            embedded_words = tf.reshape(embedded_words, shape=[s[0]*s[1], s[-2], self.char_embeddings.shape[1]])

            embedded_caps = tf.nn.embedding_lookup(self.caps_embeddings, self.input_caps)
            s1 = tf.shape(embedded_caps)
            embedded_caps = tf.reshape(embedded_caps, shape=[s1[0]*s1[1], s1[-2], self.caps_embeddings.shape[1]])

        embedded_words = tf.concat([embedded_words, embedded_caps], axis=-1) # caps emb
        word_lengths_all = tf.reshape(self.words_lens, shape=[s[0]*s[1]])

        #---------------------------------------------------------------
        # Dynamic_LSTM for speed or CNN
        #---------------------------------------------------------------
        _, ((_, output_fw), (_, output_bw)) = tf.nn.bidirectional_dynamic_rnn(self.lstm_cell[0], self.lstm_cell[1], embedded_words, sequence_length=word_lengths_all, dtype=tf.float32, scope='chars')

        output = tf.concat([output_fw, output_bw], axis=-1)
        char_rep = tf.reshape(output, shape=[-1, s[1] , 2*25])
        #---------------------------------------------------------------
        '''conv_results_list = []
        for filter_width in (3, 4, 5, 7):
            conv_results_list.append(tf.layers.conv2d(embedded_words, 25, (1, filter_width), padding='same', kernel_initializer=tf.orthogonal_initializer()))

        char_rep = tf.concat(conv_results_list, axis=3)
        char_rep = tf.reduce_max(char_rep, axis=2)'''
        #---------------------------------------------------------------
        word_embeddings = tf.concat([embedded_sequence, char_rep], axis=-1)
        word_embeddings = variational_dropout(word_embeddings, 0.7)
        #---------------------------------------------------------------

        with tf.name_scope('lstm'):
            rnn_outputs, states = tf.nn.bidirectional_dynamic_rnn(self.bidir_lstm[0], self.bidir_lstm[1], word_embeddings, sequence_length=self.input_lens, dtype=tf.float32, scope='words')
            rnn_outputs = tf.concat(rnn_outputs, axis=2)
            
        logits = [tf.layers.dense(rnn_outputs, classes_count, kernel_initializer=tf.orthogonal_initializer(), kernel_regularizer=tf.nn.l2_loss) for classes_count in self.classes_counts]

        return logits

